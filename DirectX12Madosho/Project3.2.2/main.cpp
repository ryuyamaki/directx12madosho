// Direct3Dの初期化

#include <Windows.h>
#include <tchar.h>
#include <d3d12.h>
#include <dxgi1_6.h>
#include <vector>
#ifdef _DEBUG
#include <iostream>
#endif

#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")

using namespace std;

LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM laram)
{
	// ウィンドウが放棄されたら呼ばれる
	if (msg == WM_DESTROY)
	{
		PostQuitMessage(0); // OSに対してアプリケーションの終了を伝える
		return 0;
	}

	return DefWindowProc(hwnd, msg, wparam, laram); // 既定の処理を行う
}

const unsigned int window_width = 1280;
const unsigned int window_height = 720;

UINT64 _fenceVal = 0;

HRESULT result = S_OK;

ID3D12Device*		_dev = nullptr;
IDXGIFactory6*		_dxgiFactory = nullptr;
IDXGISwapChain4*	_swapchain = nullptr;
ID3D12CommandAllocator* _cmdAllocator = nullptr;
ID3D12GraphicsCommandList* _cmdList = nullptr;
ID3D12CommandQueue* _cmdQueue = nullptr;
ID3D12Fence* _fence = nullptr;

void EnableDebugLayer()
{
	ID3D12Debug* debugLayer = nullptr;
	auto result = D3D12GetDebugInterface(IID_PPV_ARGS(&debugLayer));

	debugLayer->EnableDebugLayer();
	debugLayer->Release();
}

int main()
{
	WNDCLASSEX w = {};

	w.cbSize = sizeof(WNDCLASSEX);
	w.lpfnWndProc = (WNDPROC)WindowProcedure; // コールバック関数
	w.lpszClassName = _T("DX12Sample"); // アプリケーションクラス名
	w.hInstance = GetModuleHandle(nullptr); // ハンドルの取得

	RegisterClassEx(&w); // アプリケーションクラス

	RECT wrc = { 0, 0, window_width, window_height };
	// 関数を使ってウィンドウのサイズを補正する
	AdjustWindowRect(&wrc, WS_OVERLAPPEDWINDOW, false);

	// ウィンドウオブジェクトの生成
	HWND hwnd = CreateWindow
	(
		w.lpszClassName,//クラス名指定
		_T("DX12テスト"),//タイトルバーの文字
		WS_OVERLAPPEDWINDOW,//タイトルバーと境界線があるウィンドウです
		CW_USEDEFAULT,//表示X座標はOSにお任せします
		CW_USEDEFAULT,//表示Y座標はOSにお任せします
		wrc.right - wrc.left,//ウィンドウ幅
		wrc.bottom - wrc.top,//ウィンドウ高
		nullptr,//親ウィンドウハンドル
		nullptr,//メニューハンドル
		w.hInstance,//呼び出しアプリケーションハンドル
		nullptr
	);

#ifdef _DEBUG
	EnableDebugLayer();
#endif // _DEBUG


	// DirectX12まわり初期化
	// フィーチャーレベル列挙
	D3D_FEATURE_LEVEL levels[] =
	{
		D3D_FEATURE_LEVEL_12_1,
		D3D_FEATURE_LEVEL_12_0,
		D3D_FEATURE_LEVEL_11_1,
		D3D_FEATURE_LEVEL_11_0,
	};

	// Direct3Dデバイスの初期化
	D3D_FEATURE_LEVEL fetureLevel;
	for (auto lv : levels) 
	{
		if (D3D12CreateDevice(nullptr, lv, IID_PPV_ARGS(&_dev)) == S_OK) 
		{
			fetureLevel = lv;
			break;
		}
	}

	result = CreateDXGIFactory1(IID_PPV_ARGS(&_dxgiFactory));
	result = _dev->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&_cmdAllocator));
	result = _dev->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, _cmdAllocator, nullptr, IID_PPV_ARGS(&_cmdList));

	D3D12_COMMAND_QUEUE_DESC cmdQueueDesc = {};					 // タイムアウトなし
	cmdQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;			 // アダプターを1つしか使わないときは0でよい
	cmdQueueDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL; // コマンドリストと合わせる
	cmdQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;			 // キュー生成
	result = _dev->CreateCommandQueue(&cmdQueueDesc, IID_PPV_ARGS(&_cmdQueue));

	HRESULT CreateSwapChainForHwnd(
		IUnknown* pDevice,											// コマンドキューオブジェクト
		HWND hWnd,													// ウィンドウハンドル
		const DXGI_SWAP_CHAIN_DESC1* pDesc,							// スワップチェイン設定
		const DXGI_SWAP_CHAIN_FULLSCREEN_DESC* pFullscreenDesc,		// ひとまずnullptr
		IDXGIOutput* pRestrictToOutput,								// これもnullptrで
		IDXGISwapChain1** ppSwapChain								// スワップチェインオブジェクト取得用
	);

	DXGI_SWAP_CHAIN_DESC1 swapchainDesc = {};

	swapchainDesc.Width = window_width;					// 画面解像度【幅】
	swapchainDesc.Height = window_height;				// 画面解像度【高】
	swapchainDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;	// ピクセルフォーマット
	swapchainDesc.Stereo = false;						// ステレオ表示フラグ
	swapchainDesc.SampleDesc.Count = 1;					// マルチサンプルの指定（Count = 1、Quality = 0でよい）
	swapchainDesc.SampleDesc.Quality = 0;				// 上記
	swapchainDesc.BufferUsage = DXGI_USAGE_BACK_BUFFER;	// DXGI_USAGE_BACK_BUFFERでよい
	swapchainDesc.BufferCount = 2;						// ダブルバッファーなら2よい
	// バックバッファは伸び縮み可能
	swapchainDesc.Scaling = DXGI_SCALING_STRETCH;		// DXGI_ SCALING_ STRETCH で よい 
	// フリップ後は速やかに破棄
	swapchainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD; // DXGI_ SWAP_ EFFECT_ FLIP_ DISCARD で よい 
	swapchainDesc.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED; // DXGI_ ALPHA_ MODE_ UNSPECIFIED で よい 
	// ウィンドウ⇔フルスクリーン切り替え可能
	swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH; // DXGI_ SWAP_ CHAIN_ FLAG_ ALLOW_ MODE_ SWITCH で よい

	result = _dxgiFactory -> CreateSwapChainForHwnd
	(
		_cmdQueue,
		hwnd,
		&swapchainDesc,
		nullptr,
		nullptr,
		(IDXGISwapChain1**)& _swapchain
	);

	// ディスクリプタヒープ（ディスクリプタをまとめて扱うためのメモリ領域の確保）
	D3D12_DESCRIPTOR_HEAP_DESC heapDesc = {};

	heapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;			// レンダーターゲットビュー
	heapDesc.NodeMask = 0;									// 複数GPUがある場合の識別フラグ
	heapDesc.NumDescriptors = 2;							// ディスクリプタの数。ダブルバッファーなので表と裏の2つ
	heapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;		// シェーダ側から参照する必要がないので特に指定なし。

	ID3D12DescriptorHeap* rtvHeaps = nullptr;
	result = _dev->CreateDescriptorHeap(&heapDesc, IID_PPV_ARGS(&rtvHeaps));

	// スワップチェインのメモリとひもづける（ディスクリプタとバッファーの関連付け）
	DXGI_SWAP_CHAIN_DESC swcDesc = {};

	result = _swapchain->GetDesc(&swcDesc);

	std::vector<ID3D12Resource*> _backBuffers(swcDesc.BufferCount);
	D3D12_CPU_DESCRIPTOR_HANDLE handle = rtvHeaps->GetCPUDescriptorHandleForHeapStart();
	// バッファーの数だけ繰り返し
	for (int idx = 0; idx < swcDesc.BufferCount; ++idx) 
	{
		result = _swapchain->GetBuffer(idx, IID_PPV_ARGS(&_backBuffers[idx]));
		handle.ptr += idx * _dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV); // ディスクリプタ一つ当たりのサイズをハンドルのptrに加算することでレンダーターゲットビューが生成される
		_dev->CreateRenderTargetView(_backBuffers[idx], nullptr, handle);
	}
	void CreateRenderTargetView
	(
		ID3D12Resource* pResource, // バッファー
		const D3D12_RENDER_TARGET_VIEW_DESC* pDesc, // 今回はnullptrでよい
		D3D12_CPU_DESCRIPTOR_HANDLE DestDiscriptor // ディスクリプタヒープハンドル（ディスクリプタヒープ上のビューのアドレスのようなもの）
	);

	result = _dev->CreateFence(_fenceVal, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&_fence));

	// 
	ShowWindow(hwnd, SW_SHOW);

	MSG msg = {};
	while (true)
	{
		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		// アプリケーションが終わるときにmessageがWM_QUITになる
		if (msg.message == WM_QUIT)
		{
			break;
		}

		result = _cmdAllocator->Reset();

		auto bbIdx = _swapchain->GetCurrentBackBufferIndex();

		D3D12_RESOURCE_BARRIER BarrierDesc = {};
		BarrierDesc.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
		BarrierDesc.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
		BarrierDesc.Transition.pResource = _backBuffers[bbIdx];
		BarrierDesc.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
		BarrierDesc.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
		BarrierDesc.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
		_cmdList->ResourceBarrier(1, &BarrierDesc);

		void OMSetRenderTargets(
			UINT numRTVDescriptors, // レンダー数
			const D3D12_CPU_DESCRIPTOR_HANDLE *pRTVHandles, //レンダーターゲットハンドルの先頭アドレス
			BOOL RTsSingleHandleToDescriptorRange, //複数時に連続しているか
			const D3D12_CPU_DESCRIPTOR_HANDLE *pDepthStencilDescriptor // 深度ステンシルバッファービューのハンドル
		);

		auto rtvH = rtvHeaps->GetCPUDescriptorHandleForHeapStart();
		rtvH.ptr += bbIdx * _dev->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		_cmdList->OMSetRenderTargets(1, &rtvH, false, nullptr);

		// 画面クリア
		float clearColor[] = { 1.0f, 1.0f, 0.0f, 1.0f };
		_cmdList->ClearRenderTargetView(rtvH, clearColor, 0, nullptr);

		BarrierDesc.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
		BarrierDesc.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
		_cmdList->ResourceBarrier(1, &BarrierDesc);

		// 命令のクローズ
		_cmdList->Close();

		//コマンドリストの実行
		ID3D12CommandList* cmdlists[] = { _cmdList };
		_cmdQueue->ExecuteCommandLists(1, cmdlists);

		_cmdQueue->Signal(_fence, ++_fenceVal);
		if (_fence->GetCompletedValue() != _fenceVal) 
		{
			auto event = CreateEvent(nullptr, false, false, nullptr);
			_fence->SetEventOnCompletion(_fenceVal, event);
			WaitForSingleObject(event, INFINITE);
			CloseHandle(event);
		}

		_cmdAllocator->Reset();//キューをクリア
		_cmdList->Reset(_cmdAllocator, nullptr);//再びコマンドリストをためる準備

		//フリップ
		_swapchain->Present(1, 0);
	}

	// もうクラスは使わないので登録解除
	UnregisterClass(w.lpszClassName, w.hInstance);
}