#include <Windows.h>
#ifdef _DEBUG
#include <iostream>
#endif

using namespace std;

// @brief コンソール画面にフォーマット付きの文字列を表示
// @param formatフォーマット（%dとか%fとかの）
// @param 可変長配列
// @remarks この関数はデバッグ用です。デバッグ時にしか動作しません
void DebugOutputFomatString(const char* format, ...)
{
#ifdef _DEBUG
	va_list valist;
	va_start(valist, format);
	printf(format, valist);
	va_end(valist);
#endif
}

//面倒だけど書かなあかんやつ
LRESULT WindowProcedure(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam) 
{
	if (msg == WM_DESTROY) //ウィンドウが破棄されたら呼ばれます
	{
		PostQuitMessage(0);//OSに対して「もうこのアプリは終わるんや」と伝える
		return 0;
	}
	return DefWindowProc(hwnd, msg, wparam, lparam);//規定の処理を行う
}

#ifdef _DEBUG
int main() 
{
#else
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int) 
{
#endif
	DebugOutputFomatString("Show window test.");
	getchar();
	return 0;
}